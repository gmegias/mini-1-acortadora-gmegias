import webapp
import urllib.parse
import uuid
import shelve

formulario = "<form action='' method='POST'>" \
             "<p>" "URL para reducir: <input type='text' name='url' size='40'>" "</p>" \
             "<p>" "<input type='submit' value='Send'>" "</p>" \
             "</form>"
URL_local = "http://localhost:1234/"


class RandomShort(webapp.webApp):
    dicc = shelve.open('shorts_urls')

    def parse(self, request):
        method = request.split(' ', 2)[0]
        resource = request.split(' ', 2)[1]
        body = request.split('\r\n\r\n', 2)[1]

        return (method, resource, body)

    def strdicc(self):
        listadicc = ""
        for element in self.dicc:
            listadicc = listadicc + f"<html><body><br>URL Original: <a href='{self.dicc[element]}'>{self.dicc[element]}</a>" \
                        f" - URL Acortada: <a href='{element}'>{URL_local + element}</a>"
        return listadicc + "</body></html>"

    def process(self, parsedRequest):

        (method, resourceName, body) = parsedRequest

        if resourceName == "/":
            if method == "GET":
                httpCode = "200 OK"
                htmlBody = ("<html><body><h2>" + formulario + "<h2>Lista:<br></h1>"
                            + self.strdicc() + "</h2></body></html>")
            elif method == "POST":
                # Me quedo con la parte que me interesa del cuerpo
                url = body.split("&")[0].split("=")[1]
                url_d = urllib.parse.unquote_plus(url)
                short_url = str(uuid.uuid4())[:6]
                if url_d == "":
                    httpCode = "400 Bad Request"
                    htmlBody = ("<html><body><h2>" + "<h2>Error at query string</h2>" + "</body></html>")
                else:
                    if not (url_d.startswith('http://') or url_d.startswith('https://')):
                        url_d = "https://" + url_d
                    if not (url_d in self.dicc.values()):
                        self.dicc[short_url] = url_d
                    httpCode = "200 OK"
                    htmlBody = ("<html><body><h2>" + formulario + "<h2>Lista:<br></h1>"
                                + self.strdicc() + "</h2></body></html>")
            else:
                httpCode = "404 Not Found"
                htmlBody = ("<html><body><h1>" + "<h1>Not Found</h1>" + "</body></html>")
        else:
            recurso = resourceName.split('/')[1]
            if recurso in self.dicc:
                url = self.dicc[recurso]
                httpCode = "301 Move permanently"
                htmlBody = ("<html><body>" + "<h1> Going to be redirect <meta http-equiv='refresh' content='2; URL=" + str(
                    url) + "'></body></html>")
            else:
                httpCode = "404 Not Found"
                htmlBody = ("<html><body><h2>" + "<h2>404 Not Found. Recurso no disponible</h1>" + "</body></html>")

        return (httpCode, htmlBody)



if __name__ == "__main__":
    testWebApp = RandomShort("localhost", 1234)
